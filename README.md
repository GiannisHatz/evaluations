# @universis/evaluations

Universis api server plugin for evaluations

## Installation

npm i @universis/evaluations

## Usage

Register `EvaluationService` in application services:

    # app.production.json

    "services": [
        ...,
         {
           "serviceType": "@universis/evaluations#EvaluationService"
         }
    ]

Add `EvaluateSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                   {
                     "loaderType": "@universis/evaluations#EvaluationSchemaLoader"
                   }
                ]
            }
        }
    }
     
