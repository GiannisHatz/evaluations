import { HttpNotFoundError, HttpTokenExpiredError, HttpServerError,
    TraceUtils, 
    HttpBadRequestError,
    Args,
    LangUtils} from '@themost/common';

import {
    DataConfigurationStrategy,
    DataContext,
    DataObject,
    EdmMapping,
    EdmType,
    ODataModelBuilder
} from '@themost/data';
import { HttpContext } from '@themost/web';
import { JsonFormInspector } from '@themost/form-inspector';
import EvaluationDocument from './evaluation-document-model';

const EvaluationAccessToken = require('./evaluation-access-token-model');
import {lt} from 'semver';
import util from "util";
import fs from "fs";
import moment from "moment";
const math = require('mathjs');

/**
 * @class
 */
@EdmMapping.entityType('EvaluationEvent')
class EvaluationEvent extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Get current evaluation based on the evaluation key contained in Evaluation-Key HTTP header
     * @param {HttpContext} context
     */
    @EdmMapping.func('Current', 'EvaluationEvent')
    static async getCurrent(context) {
        // get evaluation key
        const token = await EvaluationAccessToken.inspect(context, context.user && context.user.authenticationToken);
        if (token.active === false) {
            throw new HttpTokenExpiredError('Evaluation access token has been expired or is invalid.');
        }
        // get evaluation event
        // get current date
        const startDate = new Date(); // get current date with time
        const endDate = moment(new Date()).startOf('day').toDate(); // get only date format
        let evaluationEvent = await context.model('EvaluationEvent')
            .where('id').equal(token.sub)
            .and('date(startDate)').lowerOrEqual(startDate)
            .and('date(endDate)').greaterOrEqual(endDate)
            .and('eventStatus/alternateName').equal('EventOpened')
            .silent().getTypedItem();
        if (evaluationEvent == null) {
            throw new HttpNotFoundError('The specified evaluation cannot be found');
        }
        if (evaluationEvent.additionalType !== 'EvaluationEvent') {
            if (evaluationEvent.additionalType === 'ClassInstructorEvaluationEvent') {
                const event = await context.model('ClassInstructorEvaluationEvent')
                    .where('id').equal(token.sub)
                    .expand({
                        name: 'courseClassInstructor',
                        options: {
                            $expand: 'courseClass($expand=course,year,period),instructor'
                        }
                    })
                    .silent().getTypedItem();
                const courseClass=  event.courseClassInstructor.courseClass;
                const instructor = event.courseClassInstructor.instructor;
                evaluationEvent.courseClass = `${courseClass.course.displayCode}-${courseClass.title} (${courseClass.year.alternateName}-${courseClass.period.name}) `;
                evaluationEvent.instructor = `${instructor.familyName} ${instructor.givenName}`;
            } else {
                evaluationEvent = await evaluationEvent.silent().getAdditionalObject();
            }
        }
        return evaluationEvent;
    }

    @EdmMapping.func('form', 'Object')
    async getForm() {
        const form = await this.property('evaluationDocument').getItem();
        const service = this.context.getApplication().getService(function PrivateContentService(){});
        /**
         * @type {string}
         */
        const formPath = await new Promise((resolve, reject) => {
            service.resolvePhysicalPath(this.context, form, (err, physicalPath) => {
                if (err) {
                    return reject(err);
                }
                return resolve(physicalPath);
            })
        });
        const readFileAsync = util.promisify(fs.readFile);
        let content = await readFileAsync(formPath);
        if (content == null) {
            throw new HttpNotFoundError('Evaluation form cannot be found');
        }
        let buffer = Buffer.from(content,'base64');
        let result;
        result = JSON.parse(buffer.toString('utf8'));

        try {
            result.properties = result.properties || {};
            Object.assign(result.properties, {
                name: form.resultType,
                _id: form.id,
                _event: this.id
            });
            // post-processing
            let shouldUpgrade = true;
            let model = this.context.model(form.resultType);
            if (model != null) {
                shouldUpgrade = lt(model.version, form.version)
            }
            if (shouldUpgrade) {
                // load for inspector
                const inspector = new JsonFormInspector();
                model = inspector.inspect(result);
                // post processing steps
                Object.assign(model, {
                    source: form.resultType, // add source from evaluation document e.g. Evaluation1
                    view: form.resultType, // add view from evaluation document e.g. Evaluation1
                    version: model.version ,// set version (follow updates)
                    hidden: true
                });
                // add evaluation event and evaluation document associations
                // for further processing of evaluation results
                model.fields.push.apply(model.fields, [
                    {
                        name: 'evaluationEvent',
                        type: this.getModel().name,
                        editable: false,
                        nullable: false,
                        indexed: true,
                    }
                ]);
                // reset privileges
                /**
                 * @type {DataConfigurationStrategy|*}
                 */
                const dataConfiguration = this.context.getApplication()
                    .getConfiguration().getStrategy(DataConfigurationStrategy);

                model.privileges.splice(0);
                // get resultPrivileges
                const thisModel = dataConfiguration.model(this.getModel().name);

                model.privileges.push.apply(model.privileges, [
                    {
                        "mask": 15,
                        "type": "global"
                    },
                    {
                        "mask": 1,
                        "type": "global",
                        "account": "Administrators"
                    }
                ]);
                // add extra privileges
                if (Array.isArray(thisModel.resultPrivileges)) {
                    model.privileges.push.apply(model.privileges, thisModel.resultPrivileges);
                }
                dataConfiguration.setModelDefinition(model);
                // add entity set
                const builder = this.context.getApplication().getService(ODataModelBuilder);
                builder.addEntitySet(model.name, model.name);
                if (model.hidden) {
                    builder.removeEntitySet(model.name);
                }
                // update version
                form.version= model.version;
                await this.context.model('EvaluationDocument').silent().save(form);
            }
            return result;
        } catch (err) {
            TraceUtils.error(`Error while decoding evaluation form ${form.id} content.`);
            TraceUtils.error(err);
            throw new HttpServerError('An error occurred while decoding evaluation form.');
        }
    }

    @EdmMapping.func('Results', EdmType.CollectionOf('Object'))
    async getResults() {
        const form = await this.getForm();
        return this.context.model(form.properties.name).where('evaluationEvent').equal(this.id).prepare();
    }

    @EdmMapping.func('Tokens', EdmType.CollectionOf('EvaluationAccessToken'))
    async getAccessTokens() {
        return this.context.model('EvaluationAccessToken').where('evaluationEvent').equal(this.id).prepare();
    }

    @EdmMapping.func('FormAttributes', EdmType.CollectionOf('Object'))
    async getFormAttributes() {
        /**
         * @type {DataContext|*}
         */
        const context = this.context;
        const result = [];
        const form = await this.getForm();
        this.setParentLegend(form);
        // get model attributes
        /**
         * @type {DataConfigurationStrategy|*}
         */
        const dataConfiguration = context.getApplication()
            .getConfiguration().getStrategy(DataConfigurationStrategy);
        const dataTypes = dataConfiguration.dataTypes;
        const q = context.model(form.properties.name).asQueryable();
        const selectArguments = [];
        const model = dataConfiguration.model(form.properties.name);
        const attributes = model.fields;
        attributes.forEach(attribute => {
            const key = this.findByKey(form.components, attribute.name);
            if (key) {
                attribute = Object.assign(attribute, {
                    name: attribute.name, label: key.label
                });
                attribute.isNumber = false;
                attribute.legend = key.customProperties && key.customProperties.legend;
                if (Object.prototype.hasOwnProperty.call(dataTypes, attribute.type)) {
                    attribute.isNumber = dataTypes[attribute.type].type === 'number';
                }
                selectArguments.push(`${attribute.name}`);
                result.push(attribute);
            }

        });
        if (selectArguments.length) {
            const responses = await q.select.apply(q, selectArguments).where('evaluationEvent').equal(this.getId()).silent().getAllItems();
            for (let i = 0; i < result.length; i++) {
                const qElement = result[i];
                qElement.responses=[];
                if (qElement.isNumber) {
                    // add mean value and standard deviation
                    qElement.std = responses.length? math.std(responses.map(x => {
                        return x[qElement.name];
                    })):0;
                    qElement.avg = responses.length? math.mean(responses.map(x => {
                        return x[qElement.name];
                    })):0;
                } else {
                    qElement.responses = responses.map(x => {
                        return x[qElement.name];
                    });
                }
            }
        }
        return result;
    }

    findByKey(array, key) {
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if (element.key === key) {
                return element;
            } else {
                if (element.components) {
                    const found = this.findByKey(element.components, key);
                    if (found) {
                        return found;
                    }
                }
            }
        }
    }

    setParentLegend(component) {
        if (Array.isArray(component.components)) {
            component.components.forEach(item => {
                if (component.legend) {
                    item.customProperties = item.customProperties || {};
                    item.customProperties.legend = component.legend;
                }
                this.setParentLegend(item);
            });
        }
    }

    @EdmMapping.param('data', 'Object', false, true)
    @EdmMapping.action('Evaluate', 'Object')
    async evaluate(data) {
        const token = await EvaluationAccessToken.inspect(this.context, this.context.user.authenticationToken);
        if (token.active === false) {
            throw new HttpTokenExpiredError('Evaluation access token has been expired or is invalid.');
        }
        if (this.id !== token.sub) {
            throw new HttpBadRequestError('Evaluation access token is invalid');
        }
        // get evaluation form
        const form = await this.getForm();
        // get evaluation document
        if (form == null) {
            throw new HttpNotFoundError('Evaluation form cannot be found');
        }
        // get evaluation document
        const evaluationDocument = await this.context.model(EvaluationDocument)
            .where('id').equal(form.properties._id)
            .silent().getItem();
        if (evaluationDocument == null) {
            throw new HttpNotFoundError('Evaluation document cannot be found');
        }
        // assign defaults
        Object.assign(data, {
            evaluationEvent: this.id,
            evaluationDocument: evaluationDocument.id
        });
        // and finally append evaluation
        await this.context.model(evaluationDocument.resultType).on('after.save', (event, callback) => {
            const context = event.model.context;
            context.model(EvaluationAccessToken).where('access_token').equal(context.user && context.user.authenticationToken)
                .silent().getItem().then((token) => {
                    if (token == null) {
                        throw new HttpTokenExpiredError('Evaluation token is required');
                    }
                    return context.model(EvaluationAccessToken).silent().save({
                        access_token: token.access_token,
                        expires: new Date(0), // set epoch date
                        used: true
                    });
                }).then(() => {
                return callback();
            }).catch((err) => {
                TraceUtils.error('An error occurred while submitting evaluation');
                return callback(err);
            });
        }).silent().save(data);
        return data;
    }

    static async generateTokens(context, generateOptions) {
        let items = [];
        if (Array.isArray(generateOptions)) {
            let n = 0;
            let newItems;
            // eslint-disable-next-line no-unused-vars
            for(let itemGenerateOptions of generateOptions) {
                newItems = await EvaluationEvent.generateManyTokens(context, itemGenerateOptions);
                n += newItems.length;
                items.push.apply(items, newItems);
            }
            await context.model('EvaluationAccessToken').save(items);
            return n
        } else {
            items = await EvaluationEvent.generateManyTokens(context, generateOptions);
            await context.model('EvaluationAccessToken').save(items);
            return items.length;
        } 
    }
    /**
     * Generates a number of access tokens for the given event 
     * @param {DataContext} context
     * @param {{evaluationEvent: number, n: number, expires?: DateTime}} generateOptions
     */
    static async generateManyTokens(context, generateOptions) {
        const n = generateOptions.n;
        Args.check(typeof n === 'number', 'Expected a number greater than zero');
        Args.check(n > 0, 'The number of access tokens must be greater than zero');
        let maximumGenerateTokens = LangUtils.parseInt(context.getConfiguration().getSourceAt('settings/evaluation/maximumGenerateTokens'));
        if (maximumGenerateTokens === 0) {
            maximumGenerateTokens = 1000;
        }
        Args.check(n < maximumGenerateTokens, `The specified number of tokens exceeded the maximum allowed (${maximumGenerateTokens})`);
        const items = [];
        // generate tokens
        for (let index = 0; index < n; index++) {
            items.push({
                evaluationEvent: generateOptions.evaluationEvent,
                expires: generateOptions.expires
            });
        }
        return items;
    }
}
module.exports = EvaluationEvent;
