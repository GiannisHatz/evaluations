import {DataObject} from '@themost/data';

/**
 * @class
 */
declare class EvaluationEvent extends DataObject {

     
     public id: string; 

}

export = EvaluationEvent;
