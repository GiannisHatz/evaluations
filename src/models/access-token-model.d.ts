import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class AccessToken extends DataObject {

     
     /**
      * @description An  access token is a string representing an authorization issued to the client.
      */
     public access_token: string; 
     
     /**
      * @description The authorization server issues the registered client a client identifier -- a unique string representing the registration information provided by the client.
      */
     public client_id: string; 
     
     /**
      * @description The expiration date and time of this access token.
      */
     public expires?: Date; 
     
     /**
      * @description Refresh tokens are issued to the client by the authorization server and are used to obtain a new access token when the current access token becomes invalid or expires.
      */
     public refresh_token?: string; 
     
     /**
      * @description Scope is used to limit the authorization granted to the client by the resource owner.
      */
     public scope?: string; 

}

export = AccessToken;