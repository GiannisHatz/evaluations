import {EdmMapping, EdmType} from '@themost/data';
import EvaluationEvent from "./evaluation-event-model";
import {HttpForbiddenError, TraceUtils} from "@themost/common";

/**
 * @class
 */
@EdmMapping.entityType('ClassInstructorEvaluationEvent')
class ClassInstructorEvaluationEvent  extends EvaluationEvent {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.action('GenerateEvaluationTokens', 'GenerateInstructorTokenAction')
     async generateEvaluationTokens() {
        const context = this.context;
        const action = {
            event: this.id,
            numberOfStudents: 0,
            total: 0,
            actionStatus: {alternateName: 'PotentialActionStatus'}
        };
        // get courseClass instructor
        const event = await context.model('ClassInstructorEvaluationEvent').where('id').equal(this.id)
            .expand('courseClassInstructor')
            .getItem();
        if (event) {
            //check if a GenerateInstructorTokenAction exists
            const prevAction = await context.model('GenerateInstructorTokenAction').where('event').equal(this.id)
                .and('actionStatus/alternateName').notEqual('FailedActionStatus')
                .silent().getItem();
            if (prevAction) {
                throw new Error('A previous action for generating evaluation tokens has been found.');
            }
            const courseClass = event.courseClassInstructor && event.courseClassInstructor.courseClass;
            const eventInstructor = event.courseClassInstructor && event.courseClassInstructor.instructor;
            // get class sections
            const sections = await context.model('CourseClassSection').where('courseClass').equal(courseClass.id).getItems();
            // check if course class has sections
            if (sections && sections.length > 0 && courseClass.mustRegisterSection !== 0) {
                // get only number of students registered at specific section
                const instructorSections = await context.model('CourseClassSectionInstructor').where('courseClass').equal(courseClass.id)
                    .and('instructor').equal(eventInstructor).select('section/section as sectionId').silent().getItems();
                if (instructorSections && instructorSections.length === 0) {
                    action.actionStatus.alternateName = 'FailedActionStatus';
                    action.description = 'Instructor is not related with any of sections';
                    return await context.model('GenerateInstructorTokenAction').save(action);
                } else {
                    const sectionIds = instructorSections.map(section => {
                        return section.sectionId;
                    });
                    // get number of students for specific course class sections
                    action.numberOfStudents = await context.model('StudentCourseClass').where('courseClass').equal(courseClass.id)
                        .and('section').in(sectionIds)
                        .silent().count();
                }
            } else {
                action.numberOfStudents = await context.model('StudentCourseClass').where('courseClass').equal(courseClass.id).silent().count();
            }
            // save action and then generateTokens
            const generateAction = await context.model('GenerateInstructorTokenAction').save(action);
            const generateOptions =
                {
                    evaluationEvent: event.id,
                    n: action.numberOfStudents,
                    expires : event.endDate
                };
            try {
                // save number of students to event
                event.numberOfStudents = action.numberOfStudents;
                await context.model('ClassInstructorEvaluationEvent').save(event);
                if (event.numberOfStudents>0) {
                    await EvaluationEvent.generateTokens(context, generateOptions);
                }
                generateAction.total = action.numberOfStudents;
                generateAction.actionStatus = {alternateName: 'CompletedActionStatus'}
                return await context.model('GenerateInstructorTokenAction').save(generateAction);
            }
            catch (err) {
                TraceUtils.error(err);
                // update also action status
                generateAction.total = 0;
                generateAction.actionStatus = {alternateName: 'FailedActionStatus'}
                generateAction.description = err.message;
                return await context.model('GenerateInstructorTokenAction').save(generateAction);

            }
        }
    }
    @EdmMapping.func('TokenInfo', EdmType.CollectionOf('Object'))
    async getTokenStatistics() {
        const tokenInfo = {
            total: 0,
            used: 0,
            sent: 0
        };
        // get number if tokens
        const tokens = await this.context.model('EvaluationAccessToken').where('evaluationEvent').equal(this.getId())
            .select('count(evaluationEvent) as total', 'used', 'sent')
            .groupBy('used', 'sent', 'evaluationEvent')
            .silent().getItems();
        tokens.map(item => {
            tokenInfo.total += item.total;
            tokenInfo.used += item.used ? item.total : 0;
            tokenInfo.sent += item.sent ? item.total : 0;
        });
        return tokenInfo;
    }
}
module.exports = ClassInstructorEvaluationEvent;
