// eslint-disable-next-line no-unused-vars
import { DataObjectState } from '@themost/data';
/**
 * 
 * @param {DataEventArgs} event 
 */
async function beforeSaveAsync(event) {
    if (event.state === DataObjectState.Insert) {
        Object.assign(event.target, {
            $state: DataObjectState.Insert
        });
    }
}

/**
 * 
 * @param {DataEventArgs} event 
 */
 async function afterSaveAsync(event) {
    if (event.state === DataObjectState.Insert) {
        delete event.target.$state;
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function beforeSave(event, callback) {
    beforeSaveAsync(event).then(() => {
        return callback()
    }).catch( err => {
        return callback(err);
    })
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function afterSave(event, callback) {
    afterSaveAsync(event).then(() => {
        return callback()
    }).catch( err => {
        return callback(err);
    })
}